# Survio Api

# Prerequisite

- PHP 7.4 (There was project tested)
- composer

* SQLITE was used, so there is no need to install db

# Installation

```
# Init

git clone git@gitlab.com:tondakal/survio-api.git
cd survio-api
composer install
bin/console doctrine:migrations:migrate

# Generate SSH key

mkdir -p config/jwt

# !!! for next command use pass phrase "test" OR the same pass phrase you use write to your .env.local file JWT_PASSPHRASE={passphrase}

openssl genpkey -out config/jwt/private.pem -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
openssl pkey -in config/jwt/private.pem -out config/jwt/public.pem -pubout

# Run
symfony serve
```

# Usage

There are two schemas auth and api.
You can use prepared users with login 'master' or 'hero' and password for both is 'slave'

## Authentication and registration

url: {{ url }}/graphql/auth

### Query examples:

```
{
  login(input:{login:"master",password:"slave"}){
    token
  }
}
```

```
{
  signup(input:{login:"akalus1",password:"test", name:"Tonda"}){
    token
  }
}
```

## Authenticated data queries and mutations

url: {{ url }}/graphql/api

Before use this schema make sure you have set "Authorization" header to "Bearer {token}"

```
{
	me {
    id, login, name, projects{
      id, name, description
    }
  }
}
```

```
{
	project(id:"c5ca28f3-58c4-4cab-851d-3f4770eba6d3") {
      id, name, description
    }
}
```

```
{
  createProject(input:{name:"akalus12",description:"popis"}){
    id
  }
}
```

```
{
  updateProject(input:{id:"1332ab76-ea7f-4c7a-a16f-baf2151d93c6",name:"akalus1",description:"popis"}){
    id
  }
}
```

```
{
  deleteProject(input:{id:"F71ED114-9826-4576-80BD-CD1E10C987B5"}){
    status
  }
}
```

# INFO

There is exported Insomnia workspace in "/insomnia/survioapi.json" to easier test it
To download Insomnia go to https://insomnia.rest/download/

# TODO

- refactoring
- models
  -- add \_deleted column instead of hard delete
  -- add helpers to avoid code duplicity
- configure docker
- graphql mutation split from other queries
- graphql types updated
- write tests
- use PostgreSQL instead of SQLite
