<?php

namespace App\GraphQL\Mutation;

use App\Entity\Project;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Definition\Resolver\MutationInterface;
use Overblog\GraphQLBundle\Error\UserError;
use Overblog\GraphQLBundle\Validator\InputValidator;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Overblog\GraphQLBundle\Error\UserErrors;

class ProjectMutation implements MutationInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;
    private $em;
    private $passwordEncoder;
    private $JWTManager;
    private $validator;
    private $repository;
    protected $userRepository;
    public function __construct(EntityManagerInterface $em, ValidatorInterface $validator, JWTTokenManagerInterface $JWTManager, UserRepository $userRepository)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(Project::class);
        $this->JWTManager = $JWTManager;
        $this->validator = $validator;
        $this->userRepository = $userRepository;
    }

    /**
     * Create new project
     */
    public function createProject(Argument $args, InputValidator $inputValidator)
    {
        // input validation
        $inputValidator->validate();
        try {
            $project = new Project();
            // hydrate data from arguments to Entity
            $project = $this->getEntity($project, $args);
            // entity validation
            $errors = $this->validator->validate($project);
            if (count($errors) === 0) {
                $project->addUser($this->userRepository->getCurrentUser());
                $this->em->persist($project);
                $this->em->flush();
            }
        } catch (\Exception $e) {
            throw new UserError('Unknown error');
        }
        return $this->getProjectResponse($project, $errors);
    }
    /**
     * Create new project
     */
    public function updateProject(Argument $args, InputValidator $inputValidator)
    {
        // input validation
        $inputValidator->validate();
        $rawArgs = $args->getArrayCopy()['input'];
        $id = $rawArgs['id'] ?? null;
        $errors = null;
        $project = $this->repository->find($id);
        if (!$project) {
            throw new UserError("Project doesn't exists");
        }
        $user = $this->userRepository->getCurrentUser();
        if ($project->isProjectOfUser($user)) {
            try {

                // hydrate data from arguments to Entity
                $project = $this->getEntity($project, $args);
                // entity validation
                $errors = $this->validator->validate($project);
                if (count($errors) === 0) {
                    $this->em->persist($project);
                    $this->em->flush();
                }
            } catch (\Exception $e) {
                throw new UserError('Unknown error');
            }
        } else {
            throw new UserError("This project doesn't belong to you");
        }
        return $this->getProjectResponse($project, $errors);
    }
    /**
     * Create new project
     */
    public function deleteProject(Argument $args, InputValidator $inputValidator)
    {
        // input validation
        $inputValidator->validate();
        $rawArgs = $args->getArrayCopy()['input'];
        $id = $rawArgs['id'] ?? null;
        $errors = null;
        $project = $this->repository->find($id);
        if (!$project) {
            throw new UserError("Project doesn't exists");
        }
        $user = $this->userRepository->getCurrentUser();
        if ($project->isProjectOfUser($user)) {
            try {
                // entity validation
                $errors = $this->validator->validate($project);
                if (count($errors) === 0) {
                    $this->em->remove($project);
                    $this->em->flush();
                }
            } catch (\Exception $e) {
                throw new UserError('Unknown error');
            }
        } else {
            throw new UserError("This project doesn't belong to you");
        }
        $result = $this->getProjectResponse($project, $errors);
        return ['status' => true];
    }
    /**
     * Returns response or graphql error based on entity and its errors 
     */
    private function getProjectResponse(Project $project, $errors = null)
    {
        if (count($errors) === 0) {
            return $this->projectSuccessResponse($project);
        } else {
            $errorMessages = [];
            foreach ($errors as $error) {
                $errorMessages[] = new UserError($error->getMessage());
            }
            throw new UserErrors($errorMessages);
        }
    }
    private function getEntity(Project $project, Argument $args)
    {
        $rawArgs = $args->getArrayCopy()['input'];
        $name = $rawArgs['name'] ?? null;
        $description = $rawArgs['description'] ?? null;
        $project->setDescription($description);
        $project->setName($name);
        return $project;
    }
    private function projectSuccessResponse(Project $project): Project
    {
        return $project;
    }
}
