<?php

namespace App\GraphQL\Mutation;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Definition\Resolver\MutationInterface;
use Overblog\GraphQLBundle\Error\UserError;
use Overblog\GraphQLBundle\Validator\InputValidator;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Overblog\GraphQLBundle\Error\UserErrors;

class UserMutation implements MutationInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;
    private $em;
    private $passwordEncoder;
    private $JWTManager;
    private $validator;
    private $repository;

    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $passwordEncoder, ValidatorInterface $validator, JWTTokenManagerInterface $JWTManager)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(User::class);
        $this->passwordEncoder = $passwordEncoder;
        $this->JWTManager = $JWTManager;
        $this->validator = $validator;
    }

    /**
     * Registration of new user 
     */
    public function signup(Argument $args, InputValidator $inputValidator)
    {
        // input validation
        $inputValidator->validate();
        try {
            // hydrate data from arguments to Entity
            $user = $this->getUserData($args);
            // entity validation
            $errors = $this->validator->validate($user);
            if (count($errors) === 0) {
                $this->em->persist($user);
                $this->em->flush();
            }
        } catch (\Exception $e) {
            throw new UserError('Unknown error');
        }
        return $this->getAuthResponse($user, $errors);
    }

    /**
     * Login of existing user
     */
    public function login(Argument $args, InputValidator $inputValidator)
    {
        // input validation
        $inputValidator->validate();
        try {
            // load of user
            $rawArgs = $args->getArrayCopy()['input'];
            $login = $rawArgs['login'] ?? null;
            $password = $rawArgs['password'] ?? null;
            $user = $this->repository->findOneBy(['login' => $login]);
        } catch (\Exception $e) {
            throw new UserError('Unknown error');
        }
        // check user and his password
        if ($user) {
            $isPasswordValid = $this->passwordEncoder->isPasswordValid($user, $password);
            if ($isPasswordValid) {
                // send new token
                return $this->authSuccessResponse($user);
            } else {
                throw new UserError('Password is invalid.');
            }
        } else {
            throw new UserError('User does not exists. Please, check your credentials.');
        }
    }
    /**
     * Returns response or graphql error based on entity and its errors 
     */
    private function getAuthResponse(User $user, $errors = null)
    {
        if (count($errors) === 0) {
            return $this->authSuccessResponse($user);
        } else {
            $errorMessages = [];
            foreach ($errors as $error) {
                $errorMessages[] = new UserError($error->getMessage());
            }
            throw new UserErrors($errorMessages);
        }
    }
    private function getUserData(Argument $args)
    {
        $user = new User();
        $rawArgs = $args->getArrayCopy()['input'];
        $login = $rawArgs['login'] ?? null;
        $password = $rawArgs['password'] ?? null;
        $name = $rawArgs['name'] ?? null;
        $user->setLogin($login);
        $encodedPassword = $this->passwordEncoder->encodePassword($user, $password);
        $user->setPassword($encodedPassword);
        $user->setName($name);
        return $user;
    }
    private function authSuccessResponse($user)
    {
        return ['token' => $this->JWTManager->create($user)];
    }
}
