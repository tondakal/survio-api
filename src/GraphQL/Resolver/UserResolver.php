<?php

namespace App\GraphQL\Resolver;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use GraphQL\Type\Definition\ResolveInfo;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;

class UserResolver implements ResolverInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;
    protected $userRepository;

    public function __construct(EntityManagerInterface $em, UserRepository $userRepository)
    {
        $this->em = $em;
        $this->userRepository = $userRepository;
    }
    public function __invoke(ResolveInfo $info, $value, Argument $args)
    {
        $method = $info->fieldName;
        return $this->$method($value, $args);
    }



    public function resolve(): User
    {
        return $this->userRepository->getCurrentUser();
    }
    public function login(User $user): string
    {
        return $user->getLogin();
    }
    public function id(User $user): string
    {
        return $user->getId();
    }
    public function name(User $user): ?string
    {
        return $user->getName();
    }
    public function projects(User $user)
    {
        return $user->getProjects();
    }
}
