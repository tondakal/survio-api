<?php

namespace App\GraphQL\Resolver;

use App\Entity\Project;
use Doctrine\ORM\EntityManagerInterface;
use GraphQL\Type\Definition\ResolveInfo;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;

class ProjectResolver implements ResolverInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    public function __invoke(ResolveInfo $info, $value, Argument $args)
    {
        $method = $info->fieldName;
        return $this->$method($value, $args);
    }
    public function resolve(string $id): Project
    {
        return $this->em->getRepository(Project::class)->find($id);
    }
    public function id(Project $project): string
    {
        return $project->getId();
    }
    public function name(Project $project): string
    {
        return $project->getName();
    }
    public function description(Project $project): ?string
    {
        return $project->getDescription();
    }
}
