<?php

namespace App\Tests\Repository;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    private $repository;
    private $passwordEncoder;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->em = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        $this->repository = $this->em->getRepository(User::class);
        $this->passwordEncoder = $kernel->getContainer()->get('security.password_encoder');
        $this->em->beginTransaction();
    }

    public function testInsertValidUser()
    {
        $login = 'tonda';
        $password = 'test';
        $name = 'Antonín Kalvoda';
        // create user
        $this->createUser($login, $password, $name);
        // find newly created user
        $user = $this->repository->findOneBy(['login' => $login]);
        // check user is correct
        $this->assertNotEmpty($user, 'User was not inserted');
        $this->assertEquals($name, $user->getName(), 'User name is not as expected');
        $isPasswordValid = $this->passwordEncoder->isPasswordValid($user, $password);
        $this->assertTrue($isPasswordValid, 'Loaded password is not correct');
    }

    private function createUser($login, $password, $name): void
    {
        $user = new User();
        $user->setLogin($login);
        $encodedPassword = $this->passwordEncoder->encodePassword($user, $password);
        $user->setPassword($encodedPassword);
        $user->setName($name);
        $this->em->persist($user);
        $this->em->flush();
        $this->em->clear();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->em->rollback();
        // doing this is recommended to avoid memory leaks
        $this->em->close();
        $this->em = null;
    }
}
