<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200804134007 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE users_projects (user_id CHAR(36) NOT NULL --(DC2Type:guid)
        , project_id CHAR(36) NOT NULL --(DC2Type:guid)
        , PRIMARY KEY(user_id, project_id))');
        $this->addSql('CREATE INDEX IDX_27D2987EA76ED395 ON users_projects (user_id)');
        $this->addSql('CREATE INDEX IDX_27D2987E166D1F9C ON users_projects (project_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE users_projects');
    }
}
